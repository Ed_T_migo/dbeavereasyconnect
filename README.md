# Easy way to connect, upload to and download from DBeaver(AWS Redshit) Database

###### Note1 : The package is still under construction and may well be modified frequently

###### Note2 : Before using the package, please make sure you have the following settings
- Python : 3.8.2 64-bit
- Package : Psycopg2, Pandas and Numpy

Psycopg2 is a easy-to-use python package for connecting to PostGreSQL environment; can be installed from 
https://pypi.org/project/psycopg2/

## Details
For most of the R&D task on data analysis/modeling, it creates an easy interface to get the targeted data

###### Sample Code 
```python
import DBeaverEasyConnect as DEC 

DF = DEC.dataFetcher(
          database='yourdatabase',
          user='username',
          password='password',
          host='host',
          port='port'
          )

data = DF.select_data(
  table_name='test_db.mytable'
  , limit_rows=20
  , target_columns='column1, column2, column3'
  , other_query='where condition1 < condition2'
)
```

###### Function list

Class         | Func                   | Usage
:-------------|:---------------------- |:------------------------
dataFetcher   | select_data            | SELECT operations
dataFetcher   | update_or_delete_table | UPDATE TABLE or DELETE TABLE
dataFetcher   | create_table           | CREATE TABLE operations
dataExporter  | insert_dataframe       | upload pandas dataframe into Sql DB

###### Parameters
* dataFetcher.select_data()
  * table_name (required, string) : to indicate from which table you would like to download data
  * limit_rows (optional, int, default None) : to add 'limit x' as the query suffix
  * target_columns (optional, string, default *) : to indicate columns you want to pull 
  * other_query (optional, string, default '') : to indicate and condition you would like to use (* Note that we suggest any join operation or other complicated query be done purely in Sql or Python Pandas)
  * close_ (optional, boolean, default False) : if True, the connection will be closed after the Function
* dataFetcher.update_or_delete_table()
  * table_name (required, string) : to indicate from which table you would like to download data
  * query (required, string) : to indicate update/delete condition (e.g. 'set condition1=1 where condition2=2')
  * task (required, string) : 'UPDATE' or 'DELETE'
  * close_ (optional, boolean, default False) : if True, the connection will be closed after the Function
* dataFetcher.create_table()
  * table_name (required, string) : to indicate from which table you would like to download data
  * columns_query (required, string) : to indicate columns you would like to create (e.g. '(column1 datatype, column2 datatype)')
  * close_ (optional, boolean, default False) : if True, the connection will be closed after the Function
* dataExporter.insert_dataframe()
  * table (required, pd.DataFrame object) : the table you would like to upload
  * index_label (required, string) : column name to be primary key
  * target_table_name (required, string) : Sql path you would like to store the table at
  * sep (optional, string, default ',') : dataframe seperator
  * close_ (optional, boolean, default False) : if True, the connection will be closed after the Function
