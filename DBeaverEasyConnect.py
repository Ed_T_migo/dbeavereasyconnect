import six
import os
import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras as extras
from io import StringIO

class Connector():
  def __init__(self, database, user, password, host, port):
    self.database = database
    self.user = user
    self.password = password
    self.host = host
    self.port = port

    print(
      'Connection Info : \n' + 
      'User : ' + self.user + '\n'
      'Host-Port : ' + self.host + ':' + self.port + '\n'
    )

    try:
      self._connect()
    except:
      print('Connection Failed : Please reconfirm your connection info; Connection Will be Shutted Down')
    
  def _connect(self):
    self.conn = psycopg2.connect(
      database=self.database,
      user=self.user,
      password=self.password,
      host=self.host,
      port=self.port
    )

  def _close_connection(self):
    self.conn.close()

class dataFetcher(Connector):
  def __init__(self, database, user, password, host, port):
    super(dataFetcher, self).__init__(database, user, password, host, port)

  def select_data(self, table_name, limit_rows=None, target_columns='*', other_query='', close_=False):
    cur = self.conn.cursor()

    if other_query[0] != '':
      other_query = ' ' + other_query

    
    query = 'SELECT ' + target_columns + ' from ' + table_name + other_query

    if limit_rows:
      query = query + ' limit ' + str(limit_rows) + ';'

    print('Current Query ---------- ')
    print(query)
    print('------------------------')

    cur.execute(query)

    rows = cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    self.conn.commit()

    if close_:
      self._close_connection()

    return pd.DataFrame(rows, columns=colnames)

  def update_or_delete_table(self, table_name, query, task, close_=False):
    assert task in ['UPDATE', 'DELETE'], 'Assertion Error : task parameter must be UPDATE or DELETE'

    cur = self.conn.cursor()
    
    if query[0] != ' ':
      query = ' ' + query 

    query = task + ' ' + table_name + query + ';'

    print('Current Query ---------- ')
    print(query)
    print('------------------------')

    cur.execute(query)
    self.conn.commit()

    if task == 'UPDATE':
      print('Total Number of Rows Updated : ' + str(cur.rowcount))
    else:
      print('Total Number of Rows Deleted : ' + str(cur.rowcount))

    if close_:
      self._close_connection()

  def create_table(self, table_name, columns_query, close_=False):
    if columns_query[0] != '(' and columns_query[-1] != ')':
      columns_query = '(' + columns_query + ')'

    cur = self.conn.cursor()

    query =  'CREATE TABLE ' + table_name + ' ' + columns_query + ';'

    print('Current Query ---------- ')
    print(query)
    print('------------------------')

    try: 
      cur.execute(query)
    except:
      print('Table Creation Failed : Please make sure you wrote the corrent query and have specified PRIMARY KEY and DATA TYPES')

    self.conn.commit()
    print('Table <' + table_name + '> has been successfully created!')

    if close_:
      self._close_connection()
  
class dataExpoter(Connector):
  def __init__(self, database, user, password, host, port):
      super(dataExpoter, self).__init__(database, user, password, host, port)

  def insert_dataframe(self, table, index_label, target_table_name, sep=',', close_=False):
    buffer = StringIO()
    table.to_csv(buffer, index_label=index_label, header=False)
    buffer.seek(0)

    cursor = self.conn.cursor()

    try:
      cursor.copy_from(buffer, target_table_name, sep=sep)
      self.conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
      print('Error: %s' % error)
      self.conn.rollback()
      cursor.close()
      return 1

    print('Copy From StringIO Done')
    cursor.close()
